import React from "react";
import { connect, ConnectedProps } from 'react-redux';
import Course from "../../models/Course";
import * as courseActions from "../../redux/actions/courseActions";
import { bindActionCreators, Dispatch } from "redux";
import { RootState } from "./RootState";


/** Connector that wraps the component into a context providing it with redux state */
const connector = connect(
    function mapStateToProps(state: RootState) {
        // what state to get injected as props
        return {
            courses: state.courses
        };
    },
    function mapDispatchToProps(dispatch: Dispatch) {
        // what actions to make available as props
        return {
            // wraps each action into a dispatch call and make it available through the actions prop
            actions: bindActionCreators(courseActions, dispatch)
        }
    }
);


/** @summary Extracted return types of mapStateToProp and mapDispatchToProps
  * @see CourseViewer for the manual typing
  * @see https://react-redux.js.org/using-react-redux/static-typing#inferring-the-connected-props-automatically
  */
type PropsFromRedux = ConnectedProps<typeof connector>;

interface CoursePageProps extends PropsFromRedux {

}

interface CoursePageState {
    course: Course
}



class CoursesPage extends React.Component<CoursePageProps, CoursePageState> {

    constructor(props: CoursePageProps) {
        super(props);

        this.state = {
            course: {
                id: 0,
                title: ""
            }
        };
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h2>Courses</h2>
                <h3>Add Course</h3>
                <input type="text" onChange={this.handleChange} value={this.state.course.title}></input>
                <input type="submit" value="Save"></input>
            </form>
        );
    }

    handleSubmit = (event: any) => {
        console.debug('CoursesPage.handleSubmit');

        // prevent page reload
        event.preventDefault();

        // replaced with redux dispatch
        const id = this.nextCourseId(this.props.courses);
        const course = { ...this.state.course, id };
        this.props.actions.createCourse(course);
    }

    handleChange = (event: any) => {
        console.debug('CoursesPage.handleChange');

        const course = {
            ...this.state.course,
            title: event.target.value
        };

        this.setState({ course });
    }

    nextCourseId = (courses: Course[]): number => {
        return 1 + (courses || []).length;
    }
}


export default connector(CoursesPage);