import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
import * as couseActionCreators from '../../redux/actions/courseActions';
import React from "react";
import Course from "../../models/Course";
import { RootState } from "./RootState";


export class CourseViewer extends React.Component<Props> {

    render() {
        return (
            <table>
                {this.props.courses.map(c =>
                    <tr>
                        <td>{c.id}</td>
                        <td>{c.title}</td>
                    </tr>
                )}
            </table>
        );
    }
}

/** @summary actions from redux store */
interface DispatchProps {
    // createCourse: () => void
}

/** @summary component props */
interface OwnProps {
    // defaultPrice: number
}

/** @summary prop from redux store */
interface StateProps {
    courses: Course[]
}

type Props = StateProps
    & DispatchProps
    & OwnProps;

const reduxConnector = connect<StateProps, DispatchProps, OwnProps, RootState>(
    function mapStateToProps(state: RootState, ownProps: OwnProps): StateProps {
        return {
            courses: state.courses
        };
    },
    function mapDispatchToProps(dispatch: Dispatch, ownProps: OwnProps): DispatchProps {
        return {
            actions: bindActionCreators(couseActionCreators, dispatch)
        };
    }
);

export default reduxConnector(CourseViewer);