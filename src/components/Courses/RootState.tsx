import Course from "../../models/Course";


/** @summary redux state */
export interface RootState {
    courses: Course[];
}
