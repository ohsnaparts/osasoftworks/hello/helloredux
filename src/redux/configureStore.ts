import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers/rootReducer';
import reduxImmutableStateInvariant from "redux-immutable-state-invariant"

export function configureStore(initialState: any) {
    return createStore(rootReducer, initialState, applyMiddleware(
        // This will warn us if we accidentally mutate redux state
        reduxImmutableStateInvariant()
    ));
}