import { combineReducers } from 'redux';

// renaming is intentional
import courses from "./courseReducer";

// combines all the slices into a single cake
const rootReducer = combineReducers({
    courses
});

export default rootReducer;