import { CourseAction } from "../actions/CourseAction";

/** @summary mutates state when certain action types are 'caught' */
export default function courseReducer(state = [], action: any) {
    switch (action.type) {
        case CourseAction.CREATE_COURSE: {
            return [...state, { ...action.course }]
        }
        default: {
            return state;
        }
    }
}