import Course from "../../models/Course";
import { CourseAction } from "./CourseAction";

/** @summary Action creator */
export function createCourse(course: Course) {
    return {
        type: CourseAction.CREATE_COURSE, course
    }
}