import React from 'react';
import logo from './logo.svg';

import './App.css';
import CoursesEditor from './components/Courses/CoursesEditor'
import CourseViewer from './components/Courses/CousreViewer';

// webcomponents
import './web-components/image-spinner';


function App() {
  return (
    <div className="App">
      <header className="App-header">
        
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          <image-spinner src={logo} alt="Spinning React logo"></image-spinner>
        </a>

        <CoursesEditor></CoursesEditor>
        <CourseViewer></CourseViewer>
        
      </header>
    </div>
  );
}

export default App;
