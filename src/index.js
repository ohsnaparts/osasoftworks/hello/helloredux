import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { configureStore } from './redux/configureStore';
import { Provider as ReactProvider } from 'react-redux';
import './web-components/image-spinner';

const store = configureStore({});

ReactDOM.render((
    <ReactProvider store={store}>
        <App />
    </ReactProvider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
