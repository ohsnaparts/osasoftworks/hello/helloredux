import './image-spinner.css';
    
export class LogoSpinner extends HTMLElement {
    private static InputAttributes = {
        src: "src",
        alt: "alt"
    };

    private get src() {
        return this.getAttribute(LogoSpinner.InputAttributes.src)
    }

    private get alt() {
        return this.getAttribute(LogoSpinner.InputAttributes.alt);
    }

    connectedCallback() {
        this.innerHTML = `<img class="App-logo"src=${this.src} alt="${this.alt}" />`;
    }

}

customElements.define('image-spinner', LogoSpinner);